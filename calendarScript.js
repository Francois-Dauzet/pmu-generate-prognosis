let formulaire = document.querySelector(".container-formulaire");
let containerLink = document.querySelector(".container-generate-prognosis");
let linkContainer = document.getElementById("link-item");
let inputCodeGenerate = document.getElementById("code-generate-prognosis");
let btnGeneratePrognosis = document.getElementById("btn-generate-prognosis");
let containerResult = document.querySelector(".container-result");
let captionName = document.querySelector("caption");

let linkResult = genrateLink();
let storageCourse = new Date().toJSON().slice(0, 10);
console.log(link);
linkContainer.setAttribute("href", linkResult);

btnGeneratePrognosis.addEventListener("click", (e) => {
  if (inputCodeGenerate.value != "") {
    let result = inputCodeGenerate.value;
    result = result.replace("[", "");
    result = result.replace("]", "");
    let tabResult = result.split(",");
    console.log(tabResult);
    containerLink.classList.add("d-none");
    containerResult.classList.remove("d-none");
    captionName.innerHTML = storageCourse;
    initArray();
    function initArray() {
      const tbody = document
        .querySelector("#list-result")
        .querySelector("tbody");

      for (let i = 0; i < tabResult.length; i++) {
        const ligne = tbody.insertRow();

        let itemName = ligne.insertCell();
        itemName.innerHTML = tabResult[i].trim();

        // let itemValue = ligne.insertCell();
        // itemValue.innerHTML = tabResult[i].value + "%";
        // ligne.classList.add("add-primary-result-effect");
      }
    }
  }
  e.preventDefault();
});
// btnGeneratePrognosis.addEventListener("click", (e) => {
//   if (inputCodeGenerate.value != "") {
//     let result = inputCodeGenerate.value;
//     result = result.replace("[", "");
//     result = result.replace("]", "");
//     let tabResult = result.split(",");
//     // console.log("inputCodeGenerate.value => ", inputCodeGenerate.value);
//     console.log("tabResult => ", tabResult);
//     let data = getResultPrognosis(tabResult);
//     containerLink.classList.add("d-none");
//     containerResult.classList.remove("d-none");
//     captionName.innerHTML = storageCourse;
//     initArray();
//     function initArray() {
//       const tbody = document
//         .querySelector("#list-result")
//         .querySelector("tbody");

//       for (let i = 0; i < data.length; i++) {
//         if (data[i].value != 0 && i < 8) {
//           const ligne = tbody.insertRow();

//           let itemName = ligne.insertCell();
//           itemName.innerHTML = data[i].name;

//           let itemValue = ligne.insertCell();
//           itemValue.innerHTML = data[i].value + "%";
//           // ligne.classList.add("add-primary-result-effect");
//         }
//       }
//     }
//   }
//   e.preventDefault();
// });

function genrateLink() {
  let currentDate = new Date().toJSON().slice(0, 10);
  link = "https://www.equidia.fr/courses-hippique?date=" + currentDate;
  return link;
}

function getResultPrognosis(result) {
  let cleanResult = [];
  let jockey1 = 0;
  let jockey2 = 0;
  let jockey3 = 0;
  let jockey4 = 0;
  let jockey5 = 0;
  let jockey6 = 0;
  let jockey7 = 0;
  let jockey8 = 0;
  let jockey9 = 0;
  let jockey10 = 0;
  let jockey11 = 0;
  let jockey12 = 0;
  let jockey13 = 0;
  let jockey14 = 0;
  let jockey15 = 0;
  let jockey16 = 0;

  for (let i = 0; i < result.length; i++) {
    switch (true) {
      case result[i].trim() == "'01'":
        jockey1++;
        console.log("ok");
        break;
      case result[i].trim() == "'02'":
        jockey2++;
        console.log("ok");
        break;
      case result[i].trim() == "'03'":
        jockey3++;
        console.log("ok");
        break;
      case result[i].trim() == "'04'":
        jockey4++;
        break;
      case result[i].trim() == "'05'":
        jockey5++;
        break;
      case result[i].trim() === "'06'":
        jockey6++;
        break;
      case result[i].trim() === "'07'":
        jockey7++;
        break;
      case result[i].trim() === "'08'":
        jockey8++;
        break;
      case result[i].trim() === "'09'":
        jockey9++;
        break;
      case result[i].trim() === "'10'":
        jockey10++;
        break;
      case result[i].trim() === "'11'":
        jockey11++;
        break;
      case result[i].trim() === "'12'":
        jockey12++;
        break;
      case result[i].trim() === "'13'":
        jockey13++;
        break;
      case result[i].trim() === "'14'":
        jockey14++;
        break;
      case result[i].trim() === "'15'":
        jockey15++;
        break;
      case result[i].trim() === "'16'":
        jockey16++;
        break;
    }
  }
  cleanResult.push(
    { name: 1, value: jockey1 },
    { name: 2, value: jockey2 },
    { name: 3, value: jockey3 },
    { name: 4, value: jockey4 },
    { name: 5, value: jockey5 },
    { name: 6, value: jockey6 },
    { name: 7, value: jockey7 },
    { name: 8, value: jockey8 },
    { name: 9, value: jockey9 },
    { name: 10, value: jockey10 },
    { name: 11, value: jockey11 },
    { name: 12, value: jockey12 },
    { name: 13, value: jockey13 },
    { name: 14, value: jockey14 },
    { name: 15, value: jockey15 },
    { name: 16, value: jockey16 }
  );

  console.log("cleanResult => ", cleanResult);

  cleanResult.sort(function (a, b) {
    if (a.value > b.value) {
      return -1;
    } else {
      return 1;
    }
  });
  return cleanResult;
}
